package com.zuitt;
import java.util.Scanner;

public class WDC043_S2_A1 { public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Input year to be checked if a leap year. ");
    int year = scanner.nextInt();

    boolean isLeapYear = false;

    if (year % 4 == 0) {
        if (year % 100 != 0 || year % 400 == 0) {
            isLeapYear = true;
        }
    }

    if (isLeapYear) {
        System.out.println(year + " is a leap year.");
    } else {
        System.out.println(year + " is not a leap year.");
    }
}
}
